fin = open('jolly.in', 'r')
fout = open('jolly.out', 'w')

def isJolly(arr):
    if len(arr) == 1:
        return True
    else:
        n = arr[0]
        jumps = []
        for i in range(1, len(arr)-1):
            jumps.append(abs(arr[i]-arr[i+1]))
        jumps.sort()
        for i,jump in enumerate(jumps):
            if jump != i+1:
                return False
        return True

sequences = []
seq = [int(n) for n in fin.readline().split()]
seq2 = [int(n) for n in fin.readline().split()]
sequences.append(seq)
sequences.append(seq2)

for sequence in sequences:
    if isJolly(sequence):
        fout.write("Jolly\n")
    else:
        fout.write("Not Jolly\n")
