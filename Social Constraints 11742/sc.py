import itertools

while True:
    n, m = map(int, input().split())
    if n == 0 and m == 0:
        break
    teens = list(range(0,n))
    p1 = []
    p2 = []
    constraints = []
    for i in range(m):
        a, b, c = map(int, input().split())
        p1.append(a)
        p2.append(b)
        constraints.append(c)
    permutations = list(itertools.permutations(teens, n))
    all_count = 0
    for p in permutations:
        if p1:
            count = 0
            for i in range(len(p1)):
                p1_i = p.index(p1[i])
                p2_i = p.index(p2[i])
                distance = abs(p1_i - p2_i)
                if constraints[i] > 0:
                    if distance <= constraints[i]:
                        count += 1
                else:
                    if distance >= -1*constraints[i]:
                        count += 1
            if count == len(p1):
                all_count += 1
        else:
            all_count += 1
    print(all_count)
