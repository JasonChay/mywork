def safeRow(grid, r):
    queens = 0
    for c in range(8):
        if grid[r][c] != ' ':
            queens += 1
    if queens > 1:
        return False
    else:
        return True

def safeCol(grid, c):
    queens = 0
    for r in range(8):
        if grid[r][c] != ' ':
            queens += 1
    if queens > 1:
        return False
    else:
        return True

def safeDiagfor(grid, r, c):
    count = 1
    while True:
        if (r-count < 0 or c+count > 7) and (r+count > 7 or c-count < 0):
            break
        if r-count >= 0 and c+count <= 7:
            if grid[r-count][c+count] != ' ':
                return False
        if r+count <= 7 and c-count >= 0:
            if grid[r+count][c-count] != ' ':
                return False
        count += 1
    return True

def safeDiagback(grid, r, c):
    count = 1
    while True:
        if (r+count > 7 or c+count > 7) and (r-count < 0 or c-count < 0):
            break
        if r+count <= 7 and c+count <= 7:
            if grid[r+count][c+count] != ' ':
                return False
        if r-count >= 0 and c-count >= 0:
            if grid[r-count][c-count] != ' ':
                return False
        count += 1
    return True

def checkSolution(grid):
    for row in range(8):
        for col in range(8):
            if grid[row][col] != ' ':
                if not (safeRow(grid, row) and safeCol(grid, col) and safeDiagfor(grid, row, col) and safeDiagback(grid, row, col)):
                    return False
    return True

def refreshBoard(board, row):
    board[row] = [' ',' ',' ',' ',' ',' ',' ',' ']

board = [[' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' '],
         [' ',' ',' ',' ',' ',' ',' ',' ']]

#print(checkSolution(board))
#print(safeRow(board, 1))
#print(safeCol(board, 1))
#print(safeDiagfor(board, 0, 2))
#print(safeDiagback(board, 1, 1))

numSolutions = 0
for a in range(8):
    refreshBoard(board, 0)
    board[0][a] = 'Q'
    for b in range(8):
        refreshBoard(board, 1)
        board[1][b] = 'Q'
        for c in range(8):
            refreshBoard(board, 2)
            board[2][c] = 'Q'
            for d in range(8):
                refreshBoard(board, 3)
                board[3][d] = 'Q'
                for e in range(8):
                    refreshBoard(board, 4)
                    board[4][e] = 'Q'
                    for f in range(8):
                        refreshBoard(board, 5)
                        board[5][f] = 'Q'
                        for g in range(8):
                            refreshBoard(board, 6)
                            board[6][g] = 'Q'
                            for h in range(8):
                                refreshBoard(board, 7)
                                board[7][h] = 'Q'
                                if checkSolution(board):
                                    numSolutions += 1
print(numSolutions)
