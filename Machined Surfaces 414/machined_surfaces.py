fin = open('machined_surfaces.in', 'r')
fout =  open('machined_surfaces.out', 'w')

def count_B(s):
    counter = 0
    for i in range(len(s)):
        if s[i] == 'B':
            counter += 1
    return counter

def solve_surface(arr):
    if len(arr) == 1:
        return 0
    else:
        contact_point = max(arr)
        num_gaps = 0
        for num in arr:
            num_gaps += contact_point - num
        return num_gaps

parts = []
while True:
    n = int(fin.readline().strip())
    part = []
    if(n == 0):
        break;
    for i in range(n):
        part.append(fin.readline().strip())
    parts.append(part)

spaces = []
for part in parts:
    arr = []
    for s in part:
        arr.append(count_B(s))
    spaces.append(arr)


for arr in spaces:
    fout.write(str(solve_surface(arr)) + "\n")
