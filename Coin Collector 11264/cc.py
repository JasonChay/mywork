def withdraw(X):
    if X == 0:
        return
    for i in range(len(coins)-1, -1, -1):
        if coins[i] <= X:
            Y = coins[i]
            collection[Y] += 1
            break
    withdraw(X-Y)

def sumcoins(coins):
    sum = 0
    for i in range(len(coins)):
        sum += coins[i]
    return sum

def reset_collection():
    for i in range(len(coins)):
        collection[coins[i]] = 0

def countcoins(collection):
    count = 0
    for coin in collection:
        if collection[coin]:
            count += 1
    return count

T = int(input())
for case in range(T):
    n = int(input())
    coins = input().split()
    collection = {}
    for i in range(len(coins)):
        coins[i] = int(coins[i])
        collection[coins[i]] = 0
    max = 0
    for i in range(1, sumcoins(coins)+1):
        withdraw(i)
        count = countcoins(collection)
        if count > max:
            max = count
        reset_collection()
    print(max)
