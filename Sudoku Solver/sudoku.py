board = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]]

num_solutions = 0

def print_board(board):
    for row in board:
        print(' '.join([str(i) for i in row]))

def check_col(c, arr):
    for r in range(9):
        if board[r][c]:
            arr[board[r][c] - 1] += 1
    for n in arr:
        if n > 1:
            return False
    return True

def check_row(r, arr):
    for c in range(9):
        if board[r][c]:
            arr[board[r][c] - 1] += 1
    for n in arr:
        if n > 1:
            return False
    return True

def check_box(r, c, arr):
    br = int(r / 3) * 3
    bc = int(c / 3) * 3
    for r in range(br, br+3):
        for c in range(bc, bc+3):
            if board[r][c]:
                arr[board[r][c] - 1] += 1
    for n in arr:
        if n > 1:
            return False
    return True

def place(r, c, n):
    arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    arr[n-1] += 1
    if check_col(c, arr):
        arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        arr[n-1] += 1
        if check_row(r, arr):
            arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
            arr[n-1] += 1
            if check_box(r, c, arr):
                return True
    return False

def backtrack(r, c):
    global num_solutions, board

    if r == 9:
        num_solutions += 1
        print_board(board)
        print()
        return

    for i in range(9):
        if place(r, c, i+1):
            board[r][c] = i+1
        else:
            continue
        if c == 8:
            backtrack(r+1, 0)
        else:
            backtrack(r, c+1)
    board[r][c] = 0

backtrack(0, 0)
#board[0][0] = 1
#arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
#arr[2-1] += 1
#print(check_row(0, arr))
#arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
#arr[2-1] += 1
#print(check_col(1, arr))
#arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
#arr[2-1] += 1
#print(check_box(0, 1, arr))
#arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
#arr[2-1] += 1
#print(place(0,1,2))
#print(board)
