T = int(input())
for iter in range(T):
    N = int(input())
    field = input().strip()
    scarecrows = 0
    i = 0
    while i < N:
        if field[i] == ".":
            scarecrows += 1
            i += 2
        i += 1
    print("Case {}: {}".format(iter+1, scarecrows))
