import sys
# z = x - y
# z = B/(xy)
#
# x^2 + (x^2) + (x^2) = at most 10000
# x = 58 rounded up
#
# y * y * y = at most 10000
# y = 22 rounded up
#

def calculate(A, B, C):
    for x in range(-58, 59):
        for y in range(-22, 23):
            if x*y != 0:
                if x + y + ((B/(x*y))) == A and x * y * (As-x-y) == B and x**2 + y**2 + (B/(x*y))**2 == C:
                    ans = []
                    ans.append(x)
                    ans.append(y)
                    ans.append(x-y)
                    ans.sort()
                    for num in ans:
                        sys.stdout.write(str(num) + " ")
                    print()
                    return
    print("No solution.")
    return

N = int(input())
for case in range(N):
    A, B, C = map(int, input().split())
    calculate(A, B, C)
