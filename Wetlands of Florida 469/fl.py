import copy

dr = [-1,-1,-1, 0,0, 1,1,1]
dc = [-1, 0, 1,-1,1,-1,0,1]

def floodfill(cgrid, r, c, c1, c2):
    if r < 0 or r >= len(cgrid) or c < 0 or c >= len(cgrid[0]):
        return 0
    if cgrid[r][c] != c1:
        return 0
    cgrid[r][c] = c2
    ans = 1
    for i in range(8):
        ans += floodfill(cgrid, r + dr[i], c + dc[i], c1, c2)
    return ans

N = int(input())
#input()
for iter in range(N):
    grid = []
    input()
    while True:
        r = input()
        if r.strip() == "":
            break
        row = []
        for c in r:
            row.append(c)
        grid.append(row)
    while True:
        t = input()
        if t.strip() == "":
            break
        copygrid = copy.deepcopy(grid)
        i, j = map(int, t.split())
        print(floodfill(copygrid, i-1, j-1, "W", "X"))
