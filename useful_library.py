from queue import Queue
import math

def bfs(adj_list):
    INF = 1000000000
    distances = [INF] * len(adj_list)
    distances[0] = 0

    q = Queue()
    q.put(0)

    while not q.empty():
        u = q.get()
        print(u)
        for v in adj_list[u]:
            if dist[v] == INF:
                dist[v] = dist[u] + 1
                q.put(v)

################################################################################

def dfs(adj_list):
    INF = 1000000000
    distances = [INF] * len(adj_list)
    distances[0] = 0

    s = []
    s.append(0)

    while s:
        u = s.pop()
        print(u)
        for v in adj_list[u]:
            if dist[v] == INF:
                dist[v] = dist[u] + 1
                s.append(v)

################################################################################

def dfs_rec(u, adj_list):
    visited[u] = True
    for v in adj_list[u]:
        if not visited[v]:
            dfs(v, adj_list)

def connected_components(adj_list):
    numCC = 0
    visited = [False] * len(adj_list)
    for v in range(len(adj_list)):
        if not visited[v]:
            dfs_rec(v, adj_list)
            numCC += 1
    return numCC

################################################################################

dr = [-1,-1,-1, 0,0, 1,1,1]
dc = [-1, 0, 1,-1,1,-1,0,1]

def floodfill(grid, r, c, c1, c2):
    if r < 0 or r >= len(grid) or c < 0 or c >= len(grid[0]):
        return 0
    if grid[r][c] != c1:
        return 0
    grid[r][c] = c2
    ans = 1
    for i in range(len(dr)):
        ans += floodfill(grid, r + dr[i], c + dc[i], c1, c2)
    return ans

test = [
    [0,0,0,0,0,1],
    [0,1,1,1,0,1],
    [1,1,0,1,0,0],
    [0,1,1,1,1,0],
    [0,0,0,1,1,0],
    [0,1,0,0,1,0]
]
#print(floodfill(test, 1, 1, 1, 2))

################################################################################

def poly_horners(coef, x):
    ans = coef[-1]
    i = len(coef) - 2
    while i >= 0:
        ans = ans*x + coef[i]
        i -= 1
    return ans

#A = [19, 7, 4, 6]
#print(poly_horners(A, 4))

################################################################################

def primes_soe(n):
    arr = [True]*(n+1)
    for i in range(2, int(math.sqrt(n)+1)):
        if arr[i]:
            for j in range(i**2, n+1, i):
                arr[j] = False
    for i in range(2, n+1):
        if arr[i]:
            print(i)

################################################################################

def primality_mr(n):
    pass

################################################################################

def primality_trial(n):
    if n <= 1:
        return False
    elif n == 2:
        return True
    else:
        for i in range(2, int(math.sqrt(n))+1):
            if n % i == 0:
                return False
        return True

#print(primality_trial(947))

################################################################################

def gcd(a, b):
    if b == 0:
        return a
    return gcd(b, a % b)

################################################################################

def lcm(a, b):
    return a * (b/gcd(a,b))

################################################################################

def factors(n):
    ans = []
    for i in range(1, int((n/2) + 1)):
        if n % i == 0:
            ans.append(i)
    ans.append(n)
    return ans

################################################################################

def roman_num(n):
    if n != int(n) or n < 0:
        return "Invalid"
    ans = ""
    while n > 0:
        if n >= 1000:
            n -= 1000
            ans += "M"
        elif n >= 900:
            n -= 900
            ans += "CM"
        elif n >= 500:
            n -= 500
            ans += "D"
        elif n >= 400:
            n -= 400
            ans += "CD"
        elif n >= 100:
            n -= 100
            ans += "C"
        elif n >= 90:
            n -= 90
            ans += "XC"
        elif n >= 50:
            n -= 50
            ans += "L"
        elif n >= 40:
            n -= 40
            ans += "XL"
        elif n >= 10:
            n -= 10
            ans += "X"
        elif n >= 9:
            n -= 9
            ans += "IX"
        elif n >= 5:
            n -= 5
            ans += "V"
        elif n >= 4:
            n -= 4
            ans += "IV"
        elif n >= 1:
            n -= 1
            ans += "I"
    return ans

################################################################################
