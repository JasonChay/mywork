def combination(n):
    if n == 0:
        return []
    elif n == 1:
        return [(1,)]
    else:
        prev = combination(n-1)
        ans = []
        for com in prev:
            ans.append(com[:len(com)] + (n,))
        return ans + prev
print(combination(5))
