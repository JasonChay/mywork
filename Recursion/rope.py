import math
R = 6371000
high = math.pi/2
low = 0

while high - low > 1e-9:
    mid = (low+high)/2
    if R*math.tan(mid) - R*mid - 3 > 0:
        high = mid
    else:
        low = mid

t = R*math.tan(mid)
h = math.sqrt(t**2 + R**2)
print(h-R)
