from random import randint

def merge(a, b):
    a_index = 0
    b_index = 0
    ans = []
    for i in range(len(a) + len(b)):
        if a_index == len(a):
            ans.append(b[b_index])
            b_index += 1
        elif b_index == len(b):
            ans.append(a[a_index])
            a_index += 1
        elif a[a_index] < b[b_index]:
            ans.append(a[a_index])
            a_index += 1
        else:
            ans.append(b[b_index])
            b_index += 1
    return ans

def mergesort(A):
    if len(A) <= 1:
        return A
    mid = int(len(A)/2)
    a = A[:mid]
    b = A[mid:]
    return merge(mergesort(a), mergesort(b))

################################################################################

def partition(arr, p):
    a = []
    b = []
    for n in arr:
        if n < p:
            a.append(n)
        elif n > p:
            b.append(n)
    return a, b

def quicksort(arr):
    if len(arr) < 2:
        return arr
    #if len(arr) == 2:
    #    if arr[0] > arr[1]:
    #        arr[0], arr[1] = arr[1], arr[0]
    pivot = arr[randint(0, len(arr)-1)]
    a, b = partition(arr, pivot)
    return quicksort(a) + [pivot] + quicksort(b)

a = [9,0,8,7,4,6,3,2,1]
print(quicksort(a))
print(mergesort(a))
