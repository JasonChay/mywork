def hanoi(n, s, e):
    if n == 1:
        print(s, e)
    else:
        m = 6-s-e
        hanoi(n-1, s, m)
        print(s, e)
        hanoi(n-1, m, e)

hanoi(3, 1, 2)
