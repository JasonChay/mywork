def simulate(tank):
    gas = tank
    leaks = 0
    fuel_cons = int(events[0][3])
    initial_position = 0
    for i in range(1, len(events)):
        if gas < 0:
            return -1
        distance = int(events[i][0]) - initial_position
        gas -= fuel_cons*distance/100
        gas -= leaks*distance
        if events[i][1] == "Fuel":
            fuel_cons = int(events[i][3])
        elif events[i][1] == "Leak":
            leaks += 1
        elif events[i][1] == "Gas":
            gas = tank
        elif events[i][1] == "Mechanic":
            leaks = 0
        initial_position = int(events[i][0])
    return gas

while True:
    inp = input().split()
    if inp[0] == '0' and inp[1] == "Fuel" and inp[3] == '0':
        break
    events = []
    events.append(inp)
    while True:
        inp = input().split()
        events.append(inp)
        if inp[1] == "Goal":
            break

    high = 10000000
    low = 0
    mid = 0
    while high-low > 1e-8:
        mid = (high+low)/2
        if simulate(mid) > 0:
            high = mid
        else: low = mid
    print('{:.3f}'.format(mid))
