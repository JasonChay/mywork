while True:
    n, m = map(int, input().split())
    if n == 0 and m == 0:
        break
    dragons = []
    for i in range(n):
        dragons.append(int(input()))
    knights = []
    for i in range(m):
        knights.append(int(input()))
    dragons.sort()
    knights.sort()
    dcount = 0
    kcount = 0
    cost = 0
    if len(dragons) > len(knights):
        print("Loowater is doomed!")
    else:
        while True:
            if dcount == len(dragons):
                print(cost)
                break
            if kcount == len(knights):
                print("Loowater is doomed!")
                break
            if knights[kcount] >= dragons[dcount]:
                cost += knights[kcount]
                dcount += 1
                kcount += 1
            else:
                kcount += 1
