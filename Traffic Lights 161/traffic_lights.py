fin = open('traffic_lights.in', 'r')
fout = open('traffic_lights.out', 'w')

def traffic(arr):
    lights = []
    time = min(arr) - 5
    for i in range(len(arr)):
        lights.append(True)
    while time <= 18000:
        for n in range(len(arr)):
            x = (time/arr[n])
            if int(x) % 2 == 0: and time <= x*arr[n] - 5:
                lights[n] = True
            else:
                lights[n] = False
        if all(lights):
            return time
        time += 1
    return -1

input = []
while True:
    line = fin.readline().split()
    for i in range(len(line)):
        line[i] = int(line[i])
    if line == [0, 0, 0]:
        break
    else:
        input += line

scenario = []
for i in input:
    if i == 0:
        print(traffic(scenario))
        scenario = []
    else:
        scenario.append(i)
