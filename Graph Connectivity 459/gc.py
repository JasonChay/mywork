def dfs(u, adj_list):
    visited[u] = True
    for v in adj_list[u]:
        if not visited[v]:
            dfs(v, adj_list)

alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
N = int(input())
input()
for i in range(N):
    adj_list = []
    end = str(input().strip())
    end_i = alph.index(end)

    track = 0
    while True:
        inp = str(input())
        if inp.strip() == "":
            break
        while inp[0] != alph[track]:
            adj_list.append([])
            track += 1
        adj = []
        adj.append(alph.index(inp[1]))
        adj_list.append(adj)
        track += 1

    num_cc = 0
    visited = [False] * end_i
    for v in range(end_i):
        if not visited[v]:
            dfs(0, adj_list)
            num_cc += 1
    print(num_cc)
