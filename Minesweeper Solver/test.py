import numpy as np
from PIL import ImageGrab
import cv2

# Load an color image in default flag
#img = cv2.imread('pig.jpeg')
#cv2.imshow('img name', img)
#cv2.waitKey(2000)
"""
printscreen_pil = ImageGrab.grab()
printscreen_numpy =   np.array(printscreen_pil.getdata(),dtype='uint8').reshape((printscreen_pil.size[1],printscreen_pil.size[0],3))
"""

board = [
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
]

#cv2.waitKey(1000)
x = 682
y = 316
# Each box is 30 x 30 pixels
box = ImageGrab.grab(bbox = (x, y, x+540, y+480))
box_np = np.array(box)
frame = cv2.cvtColor(box_np, cv2.COLOR_BGR2RGB)

#          BGR VALUES 8,14
#dark green top:        [ 44 117  74]
#background tile #1:    [159 194 229]
#background tile #2:    [153 184 215]
#green background #1:   [ 81 215 170]
#green background #2:   [ 73 209 162]
#blue 1:                [210 118  25]
#green 2:               [ 67 146  70] [ 67 145  69]
#red 3:                 [ 59  63 213] [ 58  61 212]
#purple 4:              [157 125 180] [160 131 188]
#orange 5:              [  0 143 255]
#pygame, pyautogui

def print_box(frame, r, c):
    print(frame[60 + 30*r + 8][30*c + 14])

def print_board():
    for r in range(len(board)):
        print("-------------------------------------------------------------------------")
        print("|",end = " ")
        for c in range(len(board[0])-1):
            print(board[r][c], end = " ")
            print("|", end = " ")
        print(board[r][len(board[0])-1], end = " ")
        print("|")
    print("-------------------------------------------------------------------------")

for r in range(14):
    for c in range(18):
        color = frame[60 + 30*r + 8][30*c + 14]
        if color[0] == 159 and color[1] == 194 and color[2] == 229:
            board[r][c] = '.'
        elif color[0] == 153 and color[1] == 184 and color[2] == 215:
            board[r][c] = '.'
        elif color[0] == 210 and color[1] == 118 and color[2] == 25:
            board[r][c] = 1
        elif color[0] == 67 and color[1] == 146 and color[2] == 70:
            board[r][c] = 2
        elif color[0] == 67 and color[1] == 145 and color[2] == 69:
            board[r][c] = 2
        elif color[0] == 59 and color[1] == 63 and color[2] == 213:
            board[r][c] = 3
        elif color[0] == 58 and color[1] == 61 and color[2] == 212:
            board[r][c] = 3
        elif color[0] == 157 and color[1] == 125 and color[2] == 180:
            board[r][c] = 4
        elif color[0] == 160 and color[1] == 131 and color[2] == 188:
            board[r][c] = 4
        elif color[0] == 0 and color[1] == 143 and color[2] == 255:
            board[r][c] = 5
        elif color[0] == 7 and color[1] == 54 and color[2] == 242:
            board[r][c] = '#'
        else:
            if color[0] != 81 and color[1] != 215 and color[2] != 170:
                if color[0] != 73 and color[1] != 209 and color[2] != 162:
                    board[r][c] = '?'
                    print(color)

print_board()
print_box(frame, 1, 4)


#cv2.imshow('window',frame)
#cv2.waitKey(0)
#cv2.destroyAllWindows() # or cv2.destroy('img name')
